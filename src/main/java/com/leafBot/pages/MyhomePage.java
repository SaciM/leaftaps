package com.leafBot.pages;

import java.lang.reflect.Field;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.leafBot.testng.api.base.Annotations;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class MyhomePage extends Annotations{
	
	
	public  MyhomePage() // without constructor cant use page factory
	{
		PageFactory.initElements(driver, this);
	}
	//@CacheLookup
	@FindBy(how=How.LINK_TEXT, using="CRM/SFA") 
	WebElement link;
	
	@When("Click on the link")
	public MyhomePage clickLogin() {
		click(link);   
		return new CreateLeadPage();
	}
}


	


